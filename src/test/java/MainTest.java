import org.example.Main;
import org.junit.jupiter.api.Test;

import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

public class MainTest {

    String events="/home/thanuja/IdeaProjects/untitled/src/main/java/org/example/athlete_events.csv";
    String regions="/home/thanuja/IdeaProjects/untitled/src/main/java/org/example/noc_regions.csv";
    List<String []> eventsdata= Main.Readdata(events);
    List<String []> regionsdata=Main.Readdata(regions);
    @Test
    void problem1() {
        assertEquals(Main.GoldMedals(eventsdata),Main.GoldMedals(eventsdata));
    }
    @Test
    void problem2() {
        assertEquals(Main.Athlete_GoldMedals_1980(eventsdata),Main.Athlete_GoldMedals_1980(eventsdata));
    }
    @Test
    void problem3() {
        assertEquals(Main.Eventwise(eventsdata),Main.Eventwise(eventsdata));
    }
    @Test
    void problem4() {
        assertEquals(Main.Football(eventsdata),Main.Football(eventsdata));
    }
    @Test
    void problem5() {
        assertEquals(Main.WomenWinner(eventsdata),Main.WomenWinner(eventsdata));
    }
    @Test
    void problem6() {
        assertEquals(Main.MoreThanThree(eventsdata),Main.MoreThanThree(eventsdata));
    }
    @Test
    void problem7() {
        assertEquals(Main.Country(eventsdata,regionsdata),Main.Country(eventsdata,regionsdata));
    }

}
