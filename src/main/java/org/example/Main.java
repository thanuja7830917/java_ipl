package org.example;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;

import javax.sound.midi.SysexMessage;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;




//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static List<String []> Readdata(String fileName){
        try (CSVReader reader = new CSVReader(new FileReader(fileName))) {
            List<String[]>converted= reader.readAll();
            return converted;
        }
        catch (IOException | CsvException e) {
            e.printStackTrace();
        }
        return  null;
    }
    //1.display Year Wise NumberOf GoldMedals WonBy EachPlayer

    public static HashMap<String,HashMap<String,Integer>> GoldMedals(List<String []> data) {
        HashMap<String, HashMap<String, Integer>> object = new HashMap<>();
        for (int i=1;i<1000;i++) {

            String name=data.get(i)[1];
            String medal=data.get(i)[14];
            String year=data.get(i)[9];
             if(object.containsKey(name)){
                 HashMap<String,Integer> temp=object.get(name);
                 //System.out.println(temp);
                 if(temp.containsKey(year)){
                     if(medal.equals("Gold")) {
                         int val=temp.get(year);
                         temp.put(year,val+1);

                     }
                 }else{
                     if(medal.equals("Gold")) {
                         temp.put(year, 1);
                     }
                 }
                 object.put(name, temp);

             }else{
                 HashMap<String,Integer> temp = new HashMap<>();

                 if(medal.equals("Gold")){
                     temp.put(year,1);
                     object.put(name,temp);
                 }
             }

        }
        return object;
    }
    public static ArrayList<String> Athlete_GoldMedals_1980(List<String []> data){
        ArrayList<String> result= new ArrayList<>();
        for (int i=1;i< 10000;i++) {
            String name=data.get(i)[1];
            String year=data.get(i)[9];
            String Medal=data.get(i)[14];
            String age=data.get(i)[3];
            if(year.equals("1980") && Medal.equals("Gold") && Integer.parseInt(age)<30){
              result.add(name);
            }
        }

        return result;
    }

    //.event Wise Number Of Gold Silver Bronze Medals InY ear1980

    public static HashMap<String,HashMap<String,Integer>> Eventwise(List<String []> data) {
        HashMap<String, HashMap<String, Integer>> object = new HashMap<>();
        for (int i=1;i<10000;i++) {
            String event=data.get(i)[13];
            String year=data.get(i)[9];
            String Medal=data.get(i)[14];
            if(year.equals("1980")){
               if(object.containsKey(event)){
                   HashMap<String,Integer> temp=object.get(event);
                   if(!Medal.equals("NA")){
                       if(temp.containsKey(Medal)){
                           temp.put(Medal, temp.get(Medal) + 1);
                       }else{
                           temp.put(Medal, 1);
                       }
                   }
                   object.put(event, temp);
               }
               else{
                   HashMap<String,Integer> temp=new HashMap<>();
                   if(!Medal.equals("NA")) {
                       temp.put(Medal, 1);
                       object.put(event,temp);
                   }
               }
            }

        }
        return object;
    }
    //.display Gold Winner Of Football Of EveryOlympic
    public static HashMap<String,ArrayList<String>> Football(List<String []> data) {
        HashMap<String, ArrayList<String>> object = new HashMap<>();
        for (int i=1;i<10000;i++) {
            String sport=data.get(i)[12];
            String name=data.get(i)[1];
            String year=data.get(i)[9];
            String Medal=data.get(i)[14];
            if(object.containsKey(year)){
                if(Medal.equals("Gold") && sport.equals("Football")) {
                    ArrayList<String> temp = object.get(year);
                    temp.add(name);
                    object.put(year,temp);
                }

            }
            else{
                if(Medal.equals("Gold") && sport.equals("Football")) {
                    ArrayList<String> temp = new ArrayList<>();
                    temp.add(name);
                    object.put(year,temp);
                }
            }

        }

        return object;
    }
    //female Athlete Won Maximum Number Of Gold in All Olympics

    public static HashMap<String,Integer> WomenWinner(List<String []> data) {
        HashMap<String, Integer> object = new HashMap<>();
        for (int i=1;i< 20000;i++) {
            String name=data.get(i)[1];
            String Medal=data.get(i)[14];
            String sex=data.get(i)[2];
            if(sex.equals("F")&& Medal.equals("Gold")){
               if(object.containsKey(name)) {
                   int v= object.get(name)+1;
                  object.put(name, v);
               }
               else {
                   object.put(name, 1);
               }
            }
        }
        HashMap<String,Integer> finalresult=new HashMap<>();
        int max_value=Integer.MIN_VALUE;
        String name="";
        for(String key:object.keySet()){
            int value=object.get(key);
            if(value>max_value){
                max_value=value;
                name=key;
            }
        }
        finalresult.put(name,max_value);

        return finalresult;
    }

    //6.name Of Athlete who Participated In More Than Three Olympics


    public static String MoreThanThree(List<String []> data) {
        HashMap<String, Integer> object = new HashMap<>();
        for (int i=1;i<10000;i++) {
            String name=data.get(i)[1];
            if(object.containsKey(name)){
                object.put(name,object.get(name)+1);
                if(object.get(name)>3){
                    return name;
                }
            }
            else {
                object.put(name, 1);
            }
        }
        return "";
    }
    //Display the year wise, country that won the highest medals.
    public static HashMap<String,HashMap<String,Integer>> Country(List<String []> eventdata,List<String []> regionsdata){
        HashMap<String, HashMap<String,Integer>> object = new HashMap<>();
        HashMap<String,String> countries = new HashMap<>();
        for (int i=1;i<regionsdata.size();i++) {
            String NOC=regionsdata.get(i)[0];
            String country=regionsdata.get(i)[1];
            countries.put(NOC,country);
        }
        for( int j=1;j<10000;j++){
            String year=eventdata.get(j)[9];
            String country_code=eventdata.get(j)[7];
            String country_name=countries.get(country_code);
            String Medal=eventdata.get(j)[14];
            if(!Medal.equals("NA")) {
                if(object.containsKey(year)){

                    HashMap<String, Integer> temp = object.get(year);

                    if(temp.containsKey(country_code)){
                        temp.put(country_name, temp.get(country_name) + 1);
                    }
                    else{
                        temp.put(country_name, 1);
                    }
                    object.put(year, temp);
                }
                else{
                    HashMap<String,Integer> temp = new HashMap<>();
                    temp.put(country_name, 1);
                    object.put(year, temp);
                }
            }
        }
        HashMap<String,HashMap<String,Integer>> finalresult=new HashMap<>();
        for (String i:object.keySet()) {
            HashMap<String,Integer> temp = object.get(i);

            int max_value=0;
            String key="";
            for (String j:temp.keySet()) {
                 if(temp.get(j)>max_value){
                     max_value=temp.get(j);
                     key=j;
                 }

            }
            temp.clear();
            temp.put(key,max_value);
            finalresult.put(i,temp);


        }

        return object;
    }




    public static void main(String[] args) {

        String events="/home/thanuja/IdeaProjects/untitled/src/main/java/org/example/athlete_events.csv";
        String regions="/home/thanuja/IdeaProjects/untitled/src/main/java/org/example/noc_regions.csv";
        List<String []> eventsdata=Readdata(events);
        List<String []> regionsdata=Readdata(regions);

        HashMap<String,HashMap<String,Integer>>problem1=GoldMedals(eventsdata);
        System.out.println(problem1);
        ArrayList<String> goldmedal1980=Athlete_GoldMedals_1980(eventsdata);
        System.out.println(goldmedal1980);
        HashMap<String,HashMap<String,Integer>> problem3=Eventwise(eventsdata);
        System.out.println(problem3);
        HashMap<String,ArrayList<String>> problem4=Football(eventsdata);
        System.out.println(problem4);
       HashMap<String,Integer> problem5=WomenWinner(eventsdata);
       System.out.println(problem5);
       String problem6=MoreThanThree(eventsdata);
       System.out.println(problem6);
       HashMap<String,HashMap<String,Integer>> problem7=Country(eventsdata,regionsdata);
       System.out.println(problem7);




    }
}






